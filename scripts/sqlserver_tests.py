#!/usr/bin/python
# -*- coding: utf-8 -*-

# all duration results are in milliseconds

import optparse,logging,shlex,subprocess,math,os,shutil,re,psutil,signal
import multiprocessing as mp
import datetime as dt

LOG_LEVEL=logging.INFO

logger = logging.getLogger('test_sig')
logger.setLevel(LOG_LEVEL)
logger_ch = logging.StreamHandler()
formatter = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logger_ch.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
logger_ch.setLevel(LOG_LEVEL)
logger.addHandler(logger_ch)

log_bash_cmd = False

original_queries = [
# 1
    "SELECT *, GEOM.IsValidDetailed() FROM S_ADR.CODE_INSEE_CODE_POSTAL WHERE GEOM.STIsValid() = 0;",
# 2
    "SELECT * FROM S_GEO.COMMUNE WHERE GEOM.STArea() = (SELECT MAX(GEOM.STArea()) FROM S_GEO.COMMUNE);",
# 3
    "SELECT CODE_DEPT, geometry::UnionAggregate(GEOM) FROM [S_GEO].[COMMUNE] GROUP BY CODE_DEPT;",
# 4 (NB : stored in sqlserver_q4.sql)
    "WITH POINTS AS ( SELECT CAST('POINT ( 430354.933 6623007.700)' AS geometry) AS P, 1 AS N UNION ALL SELECT CAST('POINT ( 980190.133 6333111.233)' AS geometry), 2 UNION ALL SELECT CAST('POINT ( 574865.267 6909297.167)' AS geometry), 3 UNION ALL SELECT CAST('POINT ( 501587.200 6548318.933)' AS geometry), 4 UNION ALL SELECT CAST('POINT ( 444282.067 6251421.667)' AS geometry), 5 UNION ALL SELECT CAST('POINT ( 460953.333 6775817.633)' AS geometry), 6 UNION ALL SELECT CAST('POINT (1032008.400 6323716.133)' AS geometry), 7 UNION ALL SELECT CAST('POINT ( 875328.767 6866887.500)' AS geometry), 8 UNION ALL SELECT CAST('POINT ( 666398.867 6560452.500)' AS geometry), 9 UNION ALL SELECT CAST('POINT ( 354528.400 6636467.900)' AS geometry), 10 UNION ALL SELECT CAST('POINT (1251250.400 6464646.125)' AS geometry), 11 UNION ALL SELECT CAST('POINT ( 315060.950 6874532.888)' AS geometry), 12 UNION ALL SELECT CAST('POINT ( 415263.920 7077070.707)' AS geometry), 13 UNION ALL SELECT CAST('POINT ( 559988.123 6050300.400)' AS geometry), 14 UNION ALL SELECT CAST('POINT ( 334455.250 6333111.200)' AS geometry), 15 UNION ALL SELECT CAST('POINT ( 312459.258 7172737.333)' AS geometry), 16 ), DISTANCES AS ( SELECT N, P, [ID_GEOFLA], [INSEE_COM], [NOM_COM], GEOM, P.STDistance(GEOM) AS D, RANK() OVER(PARTITION BY N ORDER BY P.STDistance(GEOM)) AS R FROM [S_GEO].[COMMUNE] CROSS JOIN POINTS ) SELECT *, CASE D WHEN 0 THEN 'intérieur' ELSE 'extérieur' END AS SITUATION FROM DISTANCES WHERE R = 1;",
# 5
    "SELECT D1.NOM_DEPT + ' / ' + D2.NOM_DEPT AS NOMS, D1.GEOM.STUnion(D2.GEOM) AS GEO FROM [S_GEO].[DEPARTEMENT] AS D1 JOIN [S_GEO].[DEPARTEMENT] AS D2 ON D1.GEOM.STDistance(D2.GEOM) < 5000 AND D1.GEOM.STTouches(D2.GEOM) = 0 AND D1.[GID] < D2.GID;",
# 6
    "SELECT CODE_COM, NOM_COM, C.CODE_DEPT, C.NOM_DEPT, C.GEOM FROM S_GEO.COMMUNE AS C JOIN S_GEO.DEPARTEMENT AS D ON C.CODE_DEPT = D.CODE_DEPT AND C.GEOM.STIntersects(D.GEOM.STBoundary()) = 0 WHERE C.GEOM.STArea() > 34567890;",
# 7
    "SELECT CODE_COM, NOM_COM, C.CODE_DEPT, C.NOM_DEPT, C.GEOM FROM S_GEO.COMMUNE AS C JOIN S_GEO.DEPARTEMENT AS D ON C.CODE_DEPT = D.CODE_DEPT AND C.GEOM.STIntersects(D.GEOM.STCentroid()) = 1;",
# 8
    "WITH T AS ( SELECT CODE_COM, INSEE_COM, NOM_COM, GEOM.STArea() AS SURFACE, GEOM, RANK() OVER(ORDER BY ABS(GEOM.STArea() - 6666666)) AS RANK_DIF FROM S_GEO.COMMUNE) SELECT CODE_COM, INSEE_COM, NOM_COM, GEOM, SURFACE FROM T WHERE RANK_DIF = 1;",
# 9
    "SELECT CLASS_ADM, SUM(GEOM.STLength()) / 1000 AS LONGUEUR_KM FROM S_RTE.TRONCON_ROUTE WHERE CLASS_ADM <> 'Sans objet' GROUP BY CLASS_ADM;",
# 10
    "SELECT D.CODE_DEPT, NUM_ROUTE, SUM(TR.GEOM.STLength()) / 1000 AS LONGUEUR_KM FROM S_RTE.TRONCON_ROUTE AS TR JOIN S_GEO.DEPARTEMENT AS D ON TR.GEOM.STIntersects(D.GEOM) = 1 GROUP BY D.CODE_DEPT, NUM_ROUTE ORDER BY CODE_DEPT, NUM_ROUTE;"
]

optimised_queries = [
# 1
    "SELECT *, GEOM.IsValidDetailed() FROM S_ADR.CODE_INSEE_CODE_POSTAL WHERE GEOM.STIsValid() = 0;",
# 2
    "SELECT * FROM S_GEO.COMMUNE WHERE GEOM.STArea() = (SELECT MAX(GEOM.STArea()) FROM S_GEO.COMMUNE);",
# 3
    "SELECT CODE_DEPT, geometry::UnionAggregate(GEOM) FROM [S_GEO].[COMMUNE] GROUP BY CODE_DEPT;",
# 4
    "WITH POINTS AS ( SELECT CAST('POINT ( 430354.933 6623007.700)' AS geometry) AS P, 1 AS N UNION ALL SELECT CAST('POINT ( 980190.133 6333111.233)' AS geometry), 2 UNION ALL SELECT CAST('POINT ( 574865.267 6909297.167)' AS geometry), 3 UNION ALL SELECT CAST('POINT ( 501587.200 6548318.933)' AS geometry), 4 UNION ALL SELECT CAST('POINT ( 444282.067 6251421.667)' AS geometry), 5 UNION ALL SELECT CAST('POINT ( 460953.333 6775817.633)' AS geometry), 6 UNION ALL SELECT CAST('POINT (1032008.400 6323716.133)' AS geometry), 7 UNION ALL SELECT CAST('POINT ( 875328.767 6866887.500)' AS geometry), 8 UNION ALL SELECT CAST('POINT ( 666398.867 6560452.500)' AS geometry), 9 UNION ALL SELECT CAST('POINT ( 354528.400 6636467.900)' AS geometry), 10 UNION ALL SELECT CAST('POINT (1251250.400 6464646.125)' AS geometry), 11 UNION ALL SELECT CAST('POINT ( 315060.950 6874532.888)' AS geometry), 12 UNION ALL SELECT CAST('POINT ( 415263.920 7077070.707)' AS geometry), 13 UNION ALL SELECT CAST('POINT ( 559988.123 6050300.400)' AS geometry), 14 UNION ALL SELECT CAST('POINT ( 334455.250 6333111.200)' AS geometry), 15 UNION ALL SELECT CAST('POINT ( 312459.258 7172737.333)' AS geometry), 16 ), DISTANCES AS ( SELECT N, P, [ID_GEOFLA], [INSEE_COM], [NOM_COM], GEOM, P.STDistance(GEOM) AS D, RANK() OVER(PARTITION BY N ORDER BY P.STDistance(GEOM)) AS R FROM [S_GEO].[COMMUNE] CROSS JOIN POINTS ) SELECT *, CASE D WHEN 0 THEN 'intérieur' ELSE 'extérieur' END AS SITUATION FROM DISTANCES WHERE R = 1;",
# 5
    "SELECT D1.NOM_DEPT + ' / ' + D2.NOM_DEPT AS NOMS, D1.GEOM.STUnion(D2.GEOM) AS GEO FROM [S_GEO].[DEPARTEMENT] AS D1 JOIN [S_GEO].[DEPARTEMENT] AS D2 ON D1.GEOM.STDistance(D2.GEOM) < 5000 AND D1.GEOM.STTouches(D2.GEOM) = 0 AND D1.[GID] < D2.GID;",
# 6
    "SELECT CODE_COM, NOM_COM, C.CODE_DEPT, C.NOM_DEPT, C.GEOM FROM S_GEO.COMMUNE AS C JOIN S_GEO.DEPARTEMENT AS D ON C.CODE_DEPT = D.CODE_DEPT AND C.GEOM.STIntersects(D.GEOM.STBoundary()) = 0 WHERE C.GEOM.STArea() > 34567890;",
# 7 using CTE to compute each departement centroid
    "WITH D AS ( SELECT CODE_DEPT, GEOM.STCentroid() AS GEOM FROM S_GEO.DEPARTEMENT ) SELECT CODE_COM, NOM_COM, C.CODE_DEPT, C.NOM_DEPT, C.GEOM FROM S_GEO.COMMUNE AS C JOIN D ON C.CODE_DEPT = D.CODE_DEPT AND C.GEOM.STIntersects(D.GEOM) = 1;",
# 8
    "WITH T AS ( SELECT CODE_COM, INSEE_COM, NOM_COM, GEOM.STArea() AS SURFACE, GEOM, RANK() OVER(ORDER BY ABS(GEOM.STArea() - 6666666)) AS RANK_DIF FROM S_GEO.COMMUNE) SELECT CODE_COM, INSEE_COM, NOM_COM, GEOM, SURFACE FROM T WHERE RANK_DIF = 1;",
# 9
    "SELECT CLASS_ADM, SUM(GEOM.STLength()) / 1000 AS LONGUEUR_KM FROM S_RTE.TRONCON_ROUTE WHERE CLASS_ADM <> 'Sans objet' GROUP BY CLASS_ADM;",
# 10
    "SELECT D.CODE_DEPT, NUM_ROUTE, SUM(TR.GEOM.STLength()) / 1000 AS LONGUEUR_KM FROM S_RTE.TRONCON_ROUTE AS TR JOIN S_GEO.DEPARTEMENT AS D ON TR.GEOM.STIntersects(D.GEOM) = 1 GROUP BY D.CODE_DEPT, NUM_ROUTE ORDER BY CODE_DEPT, NUM_ROUTE;"
]

# exit on Ctrl+C
class graceful_killer:
  kill_now = False
  def __init__(self):
    signal.signal(signal.SIGINT, self.exit_gracefully)
    signal.signal(signal.SIGTERM, self.exit_gracefully)

  def exit_gracefully(self,signum, frame):
    logger.warn("interrupt signal received")
    self.kill_now = True

killer = graceful_killer()

def main():
    usage = "usage: %prog [options]"

    parser = optparse.OptionParser(usage=usage)

    parser.add_option("-o", "--output", dest="output_file",
                      help="the file where to output the result",
                      default="/tmp/run_tests_sqlserver.csv")
    parser.add_option("-c", "--configure", dest="config_sqlserver_env",
                      action="store_true", default=False,
                      help="do we configure the SQLServer environment")
    parser.add_option("-d", "--database", dest="database_name",
                      help="the database name", default="DB_GEO")
    parser.add_option("-p", "--password", dest="database_password",
                      help="the database password", default="3Driwacwueb")
    parser.add_option("-r", "--restore", dest="restore_database",
                      action="store_true", default=False,
                      help="do we restore the database")
    parser.add_option("-t", "--test", dest="run_tests",
                      action="store_true", default=False,
                      help="do we run the SQL tests")
    parser.add_option("--optim", dest="run_optimised_queries",
                      action="store_true", default=False,
                      help="do we run the optimised queries")
    parser.add_option("-j", "--parallel_jobs", dest="jobs_number",
                      help="do we run the parallel SQL tests", default="1")
    parser.add_option("-b", "--backup", dest="backup_database",
                      help="do we run the backup test", default=False)
    parser.add_option("-P", "--purge", dest="purge_backups",
                      action="store_true", default=False,
                      help="do we remove the created backups")
    parser.add_option("-n", "--repeat", dest="repeat_number",
                      help="how many times a query is run to measure its duration (at least 3,default:10)", default="10", type="int")
    parser.add_option("-q", "--quiet", dest="quiet", action="store_true",
                      help="do not output information", default=False)
    parser.add_option("--debug", dest="debug", action="store_true",
                      help="log all bash command call", default=False)

    (options, args) = parser.parse_args()

    # set log level
    if options.debug:
        global log_bash_cmd
        log_bash_cmd = True
        LOG_LEVEL=logging.DEBUG
    if options.quiet:
        LOG_LEVEL=logging.WARN
    if options.debug or options.quiet:
        logger.setLevel(LOG_LEVEL)
        logger_ch.setLevel(LOG_LEVEL)

    # rename the output file if already exists
    if os.path.isfile(options.output_file):
        os.rename(options.output_file, options.output_file
                  +"_"+dt.datetime.now().strftime("%y%m%d%H%M%S"))

    # get the current configuration
    env_config = get_env_config()
    with open(options.output_file, 'w') as f:
        f.write(env_config)

    # configure the database
    if options.config_sqlserver_env:
        config_sqlserver_env(options.database_name,
                             options.database_password,
                             options.jobs_number)
        logger.info("environment configuration done")

    # create and restore database
    if options.restore_database:
        duration = restore_db()
        with open(options.output_file, 'a+') as f:
            f.write("custom restore;{0:.0f}\n".format(duration))

    # run the tests
    if options.run_tests:
        logger.info("start running tests")
        queries = original_queries
        if options.run_optimised_queries:
          queries = optimised_queries
        tests_result = run_parallel_tests(options, queries)
        if killer.kill_now:
            logger.info("exit gracefully")
        with open(options.output_file, 'a+') as f:
            f.write("SQL tests\nParallel jobs;{0}\n".format(options.jobs_number))
            for query_nb,duration in tests_result:
                f.write("Test {0};{1:.0f}\n".format(query_nb,duration))
        logger.info("all tests run")

    # backup the database
    if options.backup_database:
        bck_res = backup_db(options.purge_backups)
        with open(options.output_file, 'a+') as f:
            f.write("{0} backup;{1:.0f}\n".format(bck_res[0], bck_res[1]))


def run_bash_command(command_line, grep_clock=False):
    if log_bash_cmd:
        logger.debug("run_bash_command: '{0}'".format(command_line))
    if grep_clock:
      # necessary to avoid seeing all result lines, as sqlcmd is unable to split
      # results from informational msg like the stats
      res = subprocess.check_output(command_line+"|grep Clock",shell=True)
    else:
      res = subprocess.check_output(shlex.split(command_line))
    if log_bash_cmd:
        logger.debug("run_bash_command result: '{0}'".format(res))
    return res

def get_human_readable(size,precision=2):
    suffixes=['B','KB','MB','GB','TB']
    suffixIndex = 0
    while size > 1024 and suffixIndex < 4:
        suffixIndex += 1 #increment the index of the suffix
        size = size/1024.0 #apply the division
    return "%.*f%s"%(precision,size,suffixes[suffixIndex])

def get_env_config():
    result = "Config\n"
    result += "CPU number;{0}\n".format(psutil.cpu_count())
    result += "RAM;{0}\n".format(get_human_readable(psutil.virtual_memory().total,1))
    return result

def config_sqlserver_env(database_name, database_password, jobs_number):
    os.environ['SQLCMDDBNAME'] = database_name
    logger.debug('SQLCMDDBNME set:'+ database_name)
    os.environ['SQLCMDPASSWORD'] = database_password
    logger.debug ('SQLCMDPASSWORD set');
    run_bash_command("sqlcmd -S 127.0.0.1 -U sa -Q \"EXEC sp_configure 'show advanced option', '1';RECONFIGURE;\"")
    run_bash_command("sqlcmd -S 127.0.0.1 -U sa -Q \"EXEC sp_configure 'max degree of parallelism', {0};EXEC sp_configure 'cost threshold for parallelism', 16;RECONFIGURE;\"".format(jobs_number))

def restore_db():
    n1=dt.datetime.now()
    run_bash_command("sqlcmd -S 127.0.0.1 -U sa -Q \"RESTORE DATABASE {1} FROM DISK ='db_geo_sqlserver.bak' WITH REPLACE, NORECOVERY \"")
    duration=(dt.datetime.now()-n1).total_seconds()*1000
    logger.debug("custom restore duration : {0:.0f} ms".format(duration))
    return duration

def run_test(query_nb, query, repeat_number):
    """
    Run the given query number once to help fill the cache, then run it 10 (or options.repeat_number)
    times.
    """

    # 4th query is longer than 1023 characters and overflow :
    # Msg 208, Level 16, State 1, Server sqlserver, Line 1
    # Invalid object name 'DIST塅䍅呕䉁䕌䱟䍏呁佉㵎煳捬摭'.
    # we must use a file to hold the query
    if query_nb==3:
        bash_command = "sqlcmd -p -S 127.0.0.1 -U sa -i sqlserver_q4.sql"
    else:
        #bash_command = "sqlcmd -p -S 127.0.0.1 -U sa -o /dev/null -Q \"{0}\"".format(query)
        bash_command = "sqlcmd -p -S 127.0.0.1 -U sa -Q \"{0}\" -h-1".format(query)

    try:
        logger.debug("query n°{0} dry run returns: {1}".format(query_nb, run_bash_command(bash_command, true)))
    except Exception as e:
        logger.warn("error in query n°{0}: '{2}' ({3}".format(query_nb, i, msg, e))
        return -1

    result=[]
    # run 10 times each query
    for i in range(1,repeat_number+1):
        if killer.kill_now:
            return -1
        try:
            logger.debug('Run '+str(i)+'/'+str(repeat_number))
            msg = run_bash_command(bash_command, true)
            logger.debug(msg)
            #Clock Time (ms.): total      1491  avg   1491.0 (0.7 xacts per sec.)
            m = re.split('total',msg)[1].split('avg')[0]
            duration = float(m)
            logger.debug("run query n°{0} res {1}: '{2:.0f}'".format(query_nb, i, duration))
            result.append(duration)
            logger.debug ('duration is now :'+str(result))
        except Exception as e:
            logger.warn("error in query n°{0}-{1}: '{2}' ({3}".format(query_nb, i, msg, e))
    logger.debug("query n°{0} durations: {1}".format(query_nb, result))
    return result


def run_test_args(args):
    return run_test(*args)

def run_parallel_tests(opts, queries):
    """
    run in parallel the same SQL queries in the 'queries' array.
    Compute the result on the average removing the fastest and slowest single
    result from each worker.
    """
    pool = mp.Pool(int(opts.jobs_number))
    all_tests_result = []
    for i, query in enumerate(queries):
        if killer.kill_now:
            pool.terminate()
            pool.join()
            break
        # create the arguments parallel_query_nb times
        tasks = [(i, query, opts.repeat_number)]
        tasks *= int(opts.jobs_number)
        test_results = pool.map(run_test_args, tasks)
        all_res = []
        for t_res in test_results:
            logger.debug('Results :'+str(t_res))  # doublon avec log durations + haut
            all_res.extend(t_res)
        logger.debug('Sorted results: '+str(sorted(all_res)))
        logger.debug('Kept results  : '+str(sorted(all_res)[1:-1]))
        mean = math.fsum(sorted(all_res)[1:-1])/len(all_res[1:-1])
        all_tests_result.append([i+1,mean])
        logger.info("query n°{0} average duration: {1:.0f} ms".format(i+1, mean))
    logger.info("run_parallel_tests result: '{0}'".format(all_tests_result))
    return all_tests_result

def backup_db(remove_backups=True):
    """
    backup in the database in plain SQL, custom or directory mode.
    First remove the old dump then make the backup and possibly remove the dump.
    Returns a pair with backup mode and duration
    """

    n1=dt.datetime.now()
    try: os.remove("/tmp/db_geo_sqlserver.bak")
    except: pass
    run_bash_command("time sqlcmd -S 127.0.0.1 -U sa -Q \"BACKUP DATABASE DB_GEO TO DISK = '/tmp/db_geo_sqlserver.bak'\"")
    if remove_backups:
        os.remove("/tmp/db_geo_sqlserver.sql")

    duration=(dt.datetime.now()-n1).total_seconds()*1000
    logger.info("{0} backup duration : {1:.0f} ms".format(backup_mode, duration))
    return ( backup_mode, duration )

if __name__ == "__main__":
    main()
