#!/bin/sh

#######
# to run on an Ubuntu 16.04 server

apt-get update
apt-get install -y wget ca-certificates sudo

###
# SqlServer install

MSSQL_DUMP=DB_GEO_SQLSERVER.BAK2
MSSQL_DUMP_PATH=/root/$MSSQL_DUMP
export SA_PASSWORD=uk2GliWyroon
export MSSQL_SA_PASSWORD=$SA_PASSWORD

wget --quiet -O - https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
echo "deb [arch=amd64] https://packages.microsoft.com/ubuntu/16.04/mssql-server-2017 xenial main\ndeb [arch=amd64] https://packages.microsoft.com/ubuntu/16.04/mssql-server-2017-gdr xenial main\ndeb [arch=amd64] https://packages.microsoft.com/ubuntu/16.04/prod xenial main" > /etc/apt/sources.list.d/mssql-server.list
apt-get update
apt-get install -y mssql-server mssql-tools unixodbc-dev
echo "export PATH='$PATH:/opt/mssql-tools/bin'" >> ~/.bash_profile
echo "export PATH='$PATH:/opt/mssql-tools/bin'" >> ~/.bashrc
/opt/mssql/bin/mssql-conf --noprompt setup
systemctl restart mssql-server
mkdir -p /var/opt/mssql/backup
mv $MSSQL_DUMP_PATH /var/opt/mssql/backup/
chmod 777 /var/opt/mssql/backup/$MSSQL_DUMP
/opt/mssql-tools/bin/sqlcmd -S 127.0.0.1 -U sa  -P $SA_PASSWORD -q "RESTORE DATABASE DB_GEO FROM DISK = '/var/opt/mssql/backup/$MSSQL_DUMP' WITH MOVE 'DB_GEO' TO '/var/opt/mssql/data/DB_GEO.mdf', MOVE 'DB_GEO_log' TO '/var/opt/mssql/data/DB_GEO_log.ldf'"

# test install with:
# /opt/mssql-tools/bin/sqlcmd -S 127.0.0.1 -U sa  -P $SA_PASSWORD -d DB_GEO -Q "SELECT * FROM S_GEO.COMMUNE WHERE GEOM.STArea() = (SELECT MAX(GEOM.STArea()) FROM S_GEO.COMMUNE)"

###
# PostgreSQL install

PG_DUMP=DB_GEOPG.BAK
PG_DUMP_PATH=/root/$PG_DUMP

wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
echo "deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main" > /etc/apt/sources.list.d/pgdg.list
apt-get update
apt-get install -y postgresql-10 postgis postgresql-10-postgis-2.4

cp $PG_DUMP_PATH ~postgres
chown postgres: ~postgres/$PG_DUMP
sudo -iu postgres createdb db_geo
sudo -iu postgres pg_restore -j 4 -d db_geo ~postgres/$PG_DUMP

# test install with:
# sudo -iu postgres psql -d db_geo -x -c "SELECT * FROM S_GEO.COMMUNE WHERE ST_Area(geom) = (SELECT MAX(ST_Area(GEOM)) FROM S_GEO.COMMUNE);"
