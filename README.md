# SIG comparison

Scripts used to run the SIG tests presented originally in [developpez.com](http://g-ernaelsten.developpez.com/tutoriels/comparatif-sig-et-performances/) by Frédéric Brouard.

## INSTALL

In order to run the tests on the same plateform, we use a Ubuntu 16.04.3 LTS with PostgreSQL in version 10  plus PostGIS in version 2.4 and SQL Server in version 2017.

We use the pgdg to install PostgreSQL:
```
sudo apt-get install wget ca-certificates
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
echo "deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main" > /etc/apt/sources.list.d/pgdg.list
apt-get update
apt-get install postgresql-10 postgis postgresql-10-postgis-2.4 python-psutil
```

And Microsoft for SQL Server:
```
wget https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
echo -e "deb [arch=amd64] https://packages.microsoft.com/ubuntu/16.04/mssql-server xenial main\ndeb [arch=amd64] https://packages.microsoft.com/ubuntu/16.04/prod xenial main" > /etc/apt/sources.list.d/mssql-server.list
apt-get update
apt-get install -y mssql-server mssql-tools unixodbc-dev
echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bash_profile
echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc
source ~/.bashrc
/opt/mssql/bin/mssql-conf setup
systemctl restart mssql-server
```

Get the backups for [PostgreSQL](https://1drv.ms/u/s!AqvZfiQYoNpBgXzhXmOZd24r2Bxh) and [SQL Server](https://1drv.ms/u/s!AqvZfiQYoNpBgX398-Aq8vLnon1C).

To restore the PostgreSQL database:
```
createdb db_geo
pg_restore -O -j8 -d db_geo DB_GEOPG.BAK
```

To restore the SQLServer database:
```
mkdir -p /var/opt/mssql/backup
mv DB_GEO_SQLSERVER.BAK2 /var/opt/mssql/backup/
sqlcmd -S 127.0.0.1 -U sa  -P <pwd> -q "RESTORE DATABASE DB_GEO FROM DISK = '/var/opt/mssql/backup/DB_GEO_SQLSERVER.BAK2' WITH MOVE 'DB_GEO' TO '/var/opt/mssql/data/DB_GEO.mdf', MOVE 'DB_GEO_log' TO '/var/opt/mssql/data/DB_GEO_log.ldf'"
```

The postgres user must be able to restart postgresql with no password. Use `visudo -f /etc/sudoers.d/postgres` and enter the following line :
```
postgres ALL=(ALL) NOPASSWD: /bin/systemctl restart postgresql
```

## Run the tests 


To run the tests for PostgreSQL:
```
./postgresql_tests.py-t -j <job_nb>
```

To run the tests for SQLServer:
```
./sqlserver_tests.py -c -d DB_GEO -p <pwd> -t
```
